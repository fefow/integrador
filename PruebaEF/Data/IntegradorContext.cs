﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PruebaEF.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PruebaEF.Data
{
    public class IntegradorContext:DbContext
    {
        //Empleados
        public DbSet<TipoEmpleadoModel> TipoEmpleados { get; set; }
        public DbSet<EmpleadoModel> Empleados { get; set; }
        public DbSet<UsuarioModel> Usuarios { get; set; }
        //Sector/Actividad
        public DbSet<SectorModel> Sectors { get; set; }
        public DbSet<ActividadModel> Actividads { get; set; }
        public DbSet<SectorActividadModel> SectorActividads { get; set; }
        //Piezas
        public DbSet<PiezaModel> Piezas { get; set; }
        // - en duda - public DbSet<PiezaCantidadModel> PiezaCantidads { get; set; }
        //Modelo
        public DbSet<ModeloModel> Modelos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}