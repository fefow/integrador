﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PruebaEF.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEF.ViewModels
{
    public class PiezaViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Valor { get; set; }
        public SectorActividadModel SectorActividad { get; set; }
        public ICollection<EmpleadoModel> Empleados { get; set; }
        public ICollection<PiezaComposicionModel> PiezasIntegradas { get; set; }
        public int TiempoProduccion { get; set; }
        public ICollection<EmpleadoModel> EmpleadosElegidos { get; set; }
        public ICollection<PiezaComposicionModel> PiezasElegidas { get; set; }
    }
}