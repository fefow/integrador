﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Models;
using System.Data.Entity;
using PruebaEF.Data;

namespace PruebaEF.Controllers
{
    public class SectorController : Controller
    {
        private IntegradorContext data = new IntegradorContext();

        //
        // GET: /Sector/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(data.Sectors.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }
        }

        //
        // GET: /Sector/Details/5

        public ActionResult Details(int id)
        {
            SectorModel sector = data.Sectors.Where(s => s.Id == id).First();
            return View(sector);
        }

        //
        // GET: /Sector/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Sector/Create

        [HttpPost]
        public ActionResult Create(SectorModel sector)
        {
            try
            {
                data.Sectors.Add(sector);
                data.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(sector);
            }
        }
        
        //
        // GET: /Sector/Edit/5
 
        public ActionResult Edit(int id)
        {
            SectorModel sector = data.Sectors.Where(s => s.Id == id).First();
            return View(sector);
        }

        //
        // POST: /Sector/Edit/5

        [HttpPost]
        public ActionResult Edit(SectorModel sector)
        {
            try
            {
                data.Entry(sector).State = EntityState.Modified;
                data.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View(sector);
            }
        }

        //
        // GET: /Sector/Delete/5
 
        public ActionResult Delete(int id)
        {
            SectorModel sector = data.Sectors.Where(s => s.Id == id).First();
            return View(sector);
        }

        //
        // POST: /Sector/Delete/5

        [HttpPost]
        public ActionResult Delete(SectorModel sector)
        {
            try
            {
                data.Sectors.Attach(sector);
                data.Sectors.Remove(sector);
                data.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View(sector);
            }
        }
    }
}
