﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaEF.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            if (Request.IsAuthenticated)
            {
                return View();
            }
            else {
                return RedirectToAction("LogOn","Usuario");
            }
        }

    }
}
