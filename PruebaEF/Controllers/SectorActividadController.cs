﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Data;
using PruebaEF.Models;

namespace PruebaEF.Controllers
{
    public class SectorActividadController : Controller
    {
        private IntegradorContext db = new IntegradorContext();
        //
        // GET: /SectorActividad/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.SectorActividads.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }
        }

        //
        // GET: /SectorActividad/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /SectorActividad/Create

        public ActionResult Create()
        {
            ViewBag.Sectores = db.Sectors.ToList();
            ViewBag.Actividades = db.Actividads.ToList();
            return View();
        } 

        //
        // POST: /SectorActividad/Create

        [HttpPost]
        public ActionResult Create(SectorActividadModel sa)
        {
            try
            {
                db.SectorActividads.Add(sa);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(sa);
            }
        }
        
        //
        // GET: /SectorActividad/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /SectorActividad/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SectorActividad/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /SectorActividad/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
