﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Models;
using PruebaEF.Data;
using System.Data.Entity;

namespace PruebaEF.Controllers
{
    public class TipoEmpleadoController : Controller
    {
        private IntegradorContext data = new IntegradorContext();
        //
        // GET: /TipoEmpleado/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(data.TipoEmpleados.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }
        }

        //
        // GET: /TipoEmpleado/Details/5

        public ActionResult Details(int id)
        {
            TipoEmpleadoModel model = data.TipoEmpleados.Where(t => t.Id == id).First();
            return View(model);
        }

        //
        // GET: /TipoEmpleado/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /TipoEmpleado/Create

        [HttpPost]
        public ActionResult Create(TipoEmpleadoModel tipo)
        {
            try
            {
                data.TipoEmpleados.Add(tipo);
                data.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(tipo);
            }
        }
        
        //
        // GET: /TipoEmpleado/Edit/5
 
        public ActionResult Edit(int id)
        {
            TipoEmpleadoModel model = data.TipoEmpleados.Where(t => t.Id == id).First();
            return View(model);
        }

        //
        // POST: /TipoEmpleado/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoEmpleadoModel tipo)
        {
            if(ModelState.IsValid)
            {
                data.Entry(tipo).State = EntityState.Modified;
                data.SaveChanges();

                return RedirectToAction("Index");
            }
           
            return View(tipo);
            
        }

        //
        // GET: /TipoEmpleado/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            TipoEmpleadoModel tipo = data.TipoEmpleados.Where(t => t.Id == id).First();
            return View(tipo);
        }

        //
        // POST: /TipoEmpleado/Delete/5

        [HttpPost]
        public ActionResult Delete(TipoEmpleadoModel model)
        {
            try
            {
                data.TipoEmpleados.Attach(model);
                data.TipoEmpleados.Remove(model);
                data.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
