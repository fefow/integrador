﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Data;
using PruebaEF.Models;

namespace PruebaEF.Controllers
{
    public class ActividadController : Controller
    {
        private IntegradorContext data = new IntegradorContext();
        //
        // GET: /Actividad/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(data.Actividads.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }
            
        }

        //
        // GET: /Actividad/Details/5

        public ActionResult Details(int id)
        {
            ActividadModel actividad = data.Actividads.Where(a => a.Id == id).First();
            return View(actividad);
        }

        //
        // GET: /Actividad/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Actividad/Create

        [HttpPost]
        public ActionResult Create(ActividadModel actividad)
        {
            try
            {
                data.Actividads.Add(actividad);
                data.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(actividad);
            }
        }
        
        //
        // GET: /Actividad/Edit/5
 
        public ActionResult Edit(int id)
        {
            ActividadModel actividad = data.Actividads.Where(a => a.Id == id).First();
            return View(actividad);
        }

        //
        // POST: /Actividad/Edit/5

        [HttpPost]
        public ActionResult Edit(ActividadModel actividad)
        {
            try
            {
                data.Entry(actividad).State = System.Data.Entity.EntityState.Modified;
                data.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View(actividad);
            }
        }

        //
        // GET: /Actividad/Delete/5
 
        public ActionResult Delete(int id)
        {
            ActividadModel actividad = data.Actividads.Where(a => a.Id == id).First();
            return View(actividad);
        }

        //
        // POST: /Actividad/Delete/5

        [HttpPost]
        public ActionResult Delete(ActividadModel actividad)
        {
            try
            {
                data.Actividads.Attach(actividad);
                data.Actividads.Remove(actividad);
                data.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View(actividad);
            }
        }
    }
}
