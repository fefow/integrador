﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Data;
using PruebaEF.Models;
using System.Data.Entity;

namespace PruebaEF.Controllers
{
    public class EmpleadoController : Controller
    {
        private IntegradorContext data = new IntegradorContext();
        //
        // GET: /Empleado/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(data.Empleados.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }

        }

        private List<TipoEmpleadoModel> GetTipoEmpleados() 
        {
            return data.TipoEmpleados.ToList();
        }

        //
        // GET: /Empleado/Details/5

        public ActionResult Details(string id)
        {
            EmpleadoModel empleado = (EmpleadoModel) data.Empleados.Where(e => e.Ci == id).First();
            //data.Empleados.Attach(empleado);

            return View(empleado);
        }

        //
        // GET: /Empleado/Create

        public ActionResult Create()
        {
            ViewBag.Mensaje = " Sqpdwdpdwlpd ";
            ViewBag.TipoEmpleados = GetTipoEmpleados();
            return View();
        } 

        //
        // POST: /Empleado/Create

        [HttpPost]
        public ActionResult Create(EmpleadoModel empleado)
        {
            try
            {
                if (empleado != null)
                {
                    data.Empleados.Add(empleado);
                    data.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(empleado);
            }
        }
        
        //
        // GET: /Empleado/Edit/5
 
        public ActionResult Edit(string id)
        {
            EmpleadoModel model = data.Empleados.Where(e => e.Ci == id).First();
            return View(model);
        }

        //
        // POST: /Empleado/Edit/5

        [HttpPost]
        public ActionResult Edit(EmpleadoModel empleado)
        {
            if (ModelState.IsValid)
           {
                data.Entry(empleado).State = EntityState.Modified;
                data.SaveChanges();

                return RedirectToAction("Index");
            }

                return View(empleado);
            
        }

        //
        // GET: /Empleado/Delete/5

        [HttpGet]
        public ActionResult Delete(string Id)
        {
                EmpleadoModel empleado = new EmpleadoModel { Ci = Id };
                data.Empleados.Attach(empleado);
                data.Empleados.Remove(empleado);
                data.SaveChanges();
                return RedirectToAction("Index");
        }

    }
}
