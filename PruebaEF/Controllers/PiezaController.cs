﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEF.Data;
using PruebaEF.Models;

namespace PruebaEF.Controllers
{
    public class PiezaController : Controller
    {
        private IntegradorContext data = new IntegradorContext();
        //
        // GET: /Pieza/

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return View(data.Piezas.ToList());
            }
            else
            {
                return RedirectToAction("LogOn", "Usuario");
            }
        }

        //
        // GET: /Pieza/Details/5

        public ActionResult Details(int id)
        {
            PiezaModel pieza = data.Piezas.Find(id);
            return View(pieza);
        }

        //
        // GET: /Pieza/Create

        public ActionResult Create()
        {
            var piezasDisponibles =  data.Piezas.ToList();
            
            ViewBag.Empleados = data.Empleados.ToList();
            ViewBag.SA = data.SectorActividads.ToList();
         
            var  piezasIntegradas = new List<PiezaComposicionModel>();
            foreach (var pd in piezasDisponibles)
            {
                piezasIntegradas.Add(new PiezaComposicionModel()
                {
                    Cantidad = 0,
                    Elegida = false,
                    PiezaComposicionId = pd.Id,
                    Nombre = pd.Nombre,
                    PiezaComposicion = new PiezaModel() { 
                        Id = pd.Id,
                        Nombre = pd.Nombre
                    }
                });
            }
            ViewBag.Piezas = piezasIntegradas;
           
            return View();
        } 

        //
        // POST: /Pieza/Create

        [HttpPost]
        public ActionResult Create(PiezaModel pieza)
        {
            try
            {
                pieza.PiezasIntegradas = pieza.PiezasIntegradas.Where(pi => pi.Elegida).ToList();
                //pieza.ToString();
                data.Piezas.Add(pieza);
                data.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Pieza/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Pieza/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Pieza/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Pieza/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
