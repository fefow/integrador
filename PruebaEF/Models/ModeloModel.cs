﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
    public class ModeloModel
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<PiezaCantidadModel> PiezasCantidad { get; set; }
        public ICollection<int> PiezasCantidadId { get; set; }
    }
}