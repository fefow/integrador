﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
   public class TipoEmpleadoModel
    {
        [Key]
        public int Id { get; set; }
        public float ValorHora { get; set; }
    }
}
