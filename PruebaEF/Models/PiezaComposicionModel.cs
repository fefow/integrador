﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEF.Models
{
    public class PiezaComposicionModel
    {
        [Key]
        public int Id { get; set; }
        public virtual PiezaModel PiezaComposicion { get; set; }
        public int PiezaComposicionId { get; set; }
        public int Cantidad { get; set; }
        [NotMapped]
        public bool Elegida { get; set; }
        [NotMapped]
        public string Nombre { get; set; }

       
    }
}