﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
    public class EmpleadoModel
    {
        [Key]
        public string Ci { get; set; }
       
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public int Telefono { get; set; }
        public virtual TipoEmpleadoModel TipoEmpleado { get; set; }
        public int TipoEmpleadoId { get; set; }
    }
}