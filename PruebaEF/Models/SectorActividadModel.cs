﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
    public class SectorActividadModel
    {
        [Key]
        public string Codigo { get; set; }
        public virtual SectorModel Sector { get; set; }
        public int SectorId { get; set; }
        public virtual ActividadModel Actividad { get; set; }
        public int ActividadId { get; set; }
    }
}