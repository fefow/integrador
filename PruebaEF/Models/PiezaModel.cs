﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEF.Models
{
    public class PiezaModel
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Valor { get; set; }
        public virtual SectorActividadModel SectorActividad { get; set; }
        public string SectorActividadCodigo { get; set; }
        public virtual ICollection<EmpleadoModel> Empleados { get; set; }
        public ICollection<string> EmpleadosId { get; set; }
        public virtual ICollection<PiezaComposicionModel> PiezasIntegradas { get; set; }
        public ICollection<int> PiezasIntegradasId { get; set; }
        public int TiempoProduccion { get; set; }
        [NotMapped]
        public ICollection<EmpleadoModel> EmpleadosElegidos { get; set; }
        [NotMapped]
        public ICollection<PiezaComposicionModel> PiezasElegidas { get; set; }
    }
}