﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
    public class ActividadModel
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}