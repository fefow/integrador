﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PruebaEF.Models
{
    public class PiezaCantidadModel
    {
        [Key]
        public int Id { get; set;}
        public virtual PiezaModel Pieza { get; set; }
        public int PiezaId { get; set; }
        public int Cantidad { get; set;}
        
    }
}