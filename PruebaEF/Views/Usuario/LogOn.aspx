﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.UsuarioModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Identificarse</h2>
    <p>
        Porfavor ingrese su usuario y contraseña.
    </p>

    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

    <% using (Html.BeginForm("LogOn","Usuario",FormMethod.Post)) { %>
        <%: Html.ValidationSummary(true, "No se puedo lograr una conexión. Porfavor verifique.") %>
        <div>
            <fieldset>
                <legend></legend>
                
                <div class="editor-label">
                    <%: Html.LabelFor(m => m.Id,"Cedula de Identidad") %>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(m => m.Id) %>
                    <%: Html.ValidationMessageFor(m => m.Id) %>
                </div>
                
                <div class="editor-label">
                    <%: Html.LabelFor(m => m.Password,"Contraseña") %>
                </div>
                <div class="editor-field">
                    <%: Html.PasswordFor(m => m.Password) %>
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                </div>
                
                <div class="editor-label">
                    <%: Html.CheckBoxFor(m => m.RememberMe) %>
                    <%: Html.LabelFor(m => m.RememberMe,"Recordarme") %>
                </div>
                
                <p>
                    <input type="submit" value="Iniciar Sesion" />
                </p>
            </fieldset>
        </div>
    <% } %>
</asp:Content>
