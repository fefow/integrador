﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    if (Request.IsAuthenticated) {
%>
        <li><a><%: Page.User.Identity.Name %></a></li>
        <li><%: Html.ActionLink("Cerrar Sesión", "LogOff", "Usuario") %></li>
<%
    }
    else {
%><%: Html.ActionLink("Iniciar Sesion", "LogOn", "Usuario")%>
<%
    }
%>