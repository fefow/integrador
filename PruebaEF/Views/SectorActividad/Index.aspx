﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PruebaEF.Models.SectorActividadModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sector Actividad - Listado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Sector/Actividad</h2>

<p>
    <%: Html.ActionLink("Crear Nuevo Sector/Actividad", "Create", new { @class = "btn btn-primary btn-sm" })%>
</p>
<table class="table table-striped">
    <tr>
        <th>
            Codigo
        </th>
        <th>
            Sector
        </th>
        <th>
            Actividad
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Codigo) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.SectorId) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ActividadId) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id = item.Codigo }, new {@class="btn btn-default btn-sm" })%> |
            <%: Html.ActionLink("Detalles", "Details", new { id = item.Codigo }, new { @class = "btn btn-default btn-sm" })%> |
            <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Codigo }, new { @class = "btn btn-default btn-sm" })%>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
