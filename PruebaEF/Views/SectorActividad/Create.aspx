﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.SectorActividadModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Sector/Actividad</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Codigo) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Codigo) %>
            <%: Html.ValidationMessageFor(model => model.Codigo) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.SectorId) %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownListFor(model => model.SectorId, new SelectList(ViewBag.Sectores, "Id", "Nombre"), new { @class = "form-control" })%>
            <%: Html.ValidationMessageFor(model => model.SectorId) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ActividadId) %>
        </div>
        <div class="editor-field">
             <%: Html.DropDownListFor(model => model.ActividadId, new SelectList(ViewBag.Actividades, "Id", "Nombre"), new { @class = "form-control" })%>
            <%: Html.ValidationMessageFor(model => model.ActividadId) %>
        </div>

        <p>
            <input type="submit" value="Crear" class="btn btn-success"/>
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("<- Back to List", "Index", new { @class = "btn btn-default btn-sm" })%>
</div>

</asp:Content>
