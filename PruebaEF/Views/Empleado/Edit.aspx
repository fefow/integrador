﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.EmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   Empleado - Editar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Editar Empleado</legend>

        <%: Html.HiddenFor(model => model.Ci) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Direccion) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Direccion) %>
            <%: Html.ValidationMessageFor(model => model.Direccion) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Telefono) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Telefono) %>
            <%: Html.ValidationMessageFor(model => model.Telefono) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.TipoEmpleadoId) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.TipoEmpleadoId) %>
            <%: Html.ValidationMessageFor(model => model.TipoEmpleadoId) %>
        </div>

        <p>
            <input type="submit" value="Save" class="btn btn-success" />
        </p>
    </fieldset>
<% } %>

<div class="form-group">
    <%: Html.ActionLink("Back to List", "Index", new {@class="btn btn-default"}) %>
</div>

</asp:Content>
