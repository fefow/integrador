﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.EmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Details</h2>

<fieldset class="list-group">
    <legend>Información del Empleado</legend>

    <div class="list-group-item">
        <div><h4>Cedula de Identidad</h4></div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Ci) %>
        </div>
    </div>

    <div class="list-group-item">
        <div><h4>Nombre</h4></div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Nombre) %>
        </div>
    </div>

    <div class="list-group-item">
        <div><h4>Direccion</h4></div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Direccion) %>
        </div>
    </div>

    <div class="list-group-item">
        <div><h4>Telefono</h4></div>
        <div>
            <%: Html.DisplayFor(model => model.Telefono) %>
        </div>
    </div>
    <div class="list-group-item">
        <div><h4>Tipo Empleado (Id)</h4></div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.TipoEmpleadoId) %>
        </div>
    </div>
</fieldset>

    <div class="form-group">
    <%: Html.ActionLink("Editar", "Edit", new { id = Model.Ci }, new { @class="btn btn-info" })%>
    </div>
    <div class="form-group">
    <%: Html.ActionLink("<- Regresar a los Empleados", "Index", new { @class="btn btn-default" }) %>
    </div>


</asp:Content>
