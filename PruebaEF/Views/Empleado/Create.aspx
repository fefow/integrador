﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.EmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>
<%: ViewBag.Mensaje %>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm("Create", "Empleado", FormMethod.Post, new { @role="form" }))
   { %>
    <%: Html.ValidationSummary(true)%>
    
    <fieldset>
        <legend>Nuevo Empleado</legend>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Ci,"Cedula de Identidad")%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Ci)%>
                <%: Html.ValidationMessageFor(model => model.Ci)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Nombre)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Nombre)%>
                <%: Html.ValidationMessageFor(model => model.Nombre)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Direccion)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Direccion)%>
                <%: Html.ValidationMessageFor(model => model.Direccion)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Telefono)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Telefono)%>
                <%: Html.ValidationMessageFor(model => model.Telefono)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.TipoEmpleadoId)%>
            </div>
            <div class="editor-field">
                <%: Html.DropDownListFor(model => model.TipoEmpleadoId, new SelectList(ViewBag.TipoEmpleados, "Id","Id"), new {@class="form-control" })%>
                <%: Html.ValidationMessageFor(model => model.TipoEmpleadoId) %>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" value="Crear" class="btn btn-success" />
        </div>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("<- Regresar a Empleados", "Index", null, new {@class="btn btn-default" })%>
</div>

</asp:Content>
