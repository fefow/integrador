﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PruebaEF.Models.EmpleadoModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Empleados - Inicio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Empleados</h2>

<p>
    <%: Html.ActionLink("Ingresar nuevo empleado", "Create", null, new { @class="btn btn-primary"})%>
</p>
<table class="table table-striped">
    <tr>
         <th>
            Cedula
        </th>
        <th>
            Nombre
        </th>
        <th>
            Direccion
        </th>
        <th>
            Telefono
        </th>
        <th>
            Tipo Empleado (Id)
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Ci) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Direccion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Telefono) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.TipoEmpleadoId) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id = item.Ci }, new { @class = "btn btn-default btn-sm" })%>
            <%: Html.ActionLink("Detalles", "Details", new { id = item.Ci }, new { @class = "btn btn-default btn-sm" })%>
            <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Ci }, new { @class = "btn btn-default btn-sm" })%>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
