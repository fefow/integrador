﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.EmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   Empleado - Eliminar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Eliminar</h2>

<h3>Estas seguro que quieres eliminar este Empleado?</h3>
<fieldset class="list-group">
    <legend>Información del Empleado</legend>

    <div class="list-group-item">
        <div class="display-label">Cedula de Identidad</div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Ci) %>
        </div>
    </div>

    <div class="list-group-item">
        <div class="display-label">Nombre</div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Nombre) %>
        </div>
    </div>

    <div class="list-group-item">
        <div class="display-label">Direccion</div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Direccion) %>
        </div>
    </div>

    <div class="list-group-item">
        <div class="display-label">Telefono</div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.Telefono) %>
        </div>
    </div>
    <div class="list-group-item">
        <div class="display-label">Tipo Empleado (Id)</div>
        <div class="display-field">
            <%: Html.DisplayFor(model => model.TipoEmpleadoId) %>
        </div>
    </div>
</fieldset>
<% using (Html.BeginForm("Delete","Empleado",FormMethod.Post)) { %>
    <div class="form-group">
        <input type="submit" value="Delete" class="btn btn-danger" />
    </div>
    <p></p>
    <div class="form-group">
        <%: Html.ActionLink("<- Regresar a los Empleados", "Index", null, new { @class="btn btn-default"})%>
    </div>
<% } %>

</asp:Content>
