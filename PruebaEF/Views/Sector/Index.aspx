﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PruebaEF.Models.SectorModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sector - Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Sector - Index</h2>

<p>
    <%: Html.ActionLink("Crear nuevo Sector", "Create", new { @class = "btn btn-primary btn-sm" })%>
</p>
<table class="table table-striped">
    <tr>
        <th>
            Id
        </th>
        <th>
            Nombre
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Id) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id = item.Id }, new {@class="btn btn-default btn-sm" })%>
            <%: Html.ActionLink("Detalles", "Details", new { id = item.Id }, new { @class = "btn btn-default btn-sm" })%>
            <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id }, new { @class = "btn btn-default btn-sm" })%>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
