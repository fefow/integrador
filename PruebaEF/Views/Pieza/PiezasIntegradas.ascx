﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PruebaEF.Models.PiezaComposicionModel>" %>
<%@ Import Namespace="PruebaEF.Extensions" %>
<% using(Html.BeginCollectionItem("PiezasIntegradas")){ %>
    <fieldset>
        <legend></legend>

        <%: Html.HiddenFor(model => model.Id) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PiezaComposicionId) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PiezaComposicionId) %>
            <%: Html.ValidationMessageFor(model => model.PiezaComposicionId) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Cantidad) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Cantidad) %>
            <%: Html.ValidationMessageFor(model => model.Cantidad) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Elegida) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Elegida) %>
            <%: Html.ValidationMessageFor(model => model.Elegida) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
    <% } %>
<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>
