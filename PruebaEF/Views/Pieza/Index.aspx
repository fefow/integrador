﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PruebaEF.Models.PiezaModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Piezas</h2>

<p>
    <%: Html.ActionLink("Crear Pieza", "Create") %>
</p>
<table class="table table-condensed">
    <tr>
        <th>
            Nombre
        </th>
        <th>
            Valor
        </th>
        <th>
            Sector/Actividad
        </th>
        <th>
            Tiempo de Produccion(hrs)
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Valor) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.SectorActividadCodigo) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.TiempoProduccion) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { id=item.Id }) %> |
            <%: Html.ActionLink("Details", "Details", new { id=item.Id }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { id=item.Id }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
