﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.PiezaModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container">

<h2>Create</h2>
<script src="<%: Url.Content("~/Scripts/jquery.1.9.1.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm("Create", "Pieza", FormMethod.Post, new { @role = "form" }))
   { %>
    <%: Html.ValidationSummary(true)%>
    <fieldset>
        <legend>Pieza</legend>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Nombre)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Nombre)%>
                <%: Html.ValidationMessageFor(model => model.Nombre)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Valor)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.Valor)%>
                <%: Html.ValidationMessageFor(model => model.Valor)%>
            </div>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.SectorActividadCodigo)%>
            </div>
        
            <div class="editor-field">
                <%: Html.DropDownListFor(model => model.SectorActividadCodigo, new SelectList(ViewBag.SA, "Codigo", "Codigo"), new { @class = "form-control" })%>
            </div>
        </div>
        <div class="form-group-lg">
        <fieldset>
            <legend><h4>Agregar Empleados a la Pieza</h4></legend>
            <div class="col-md-12">
                <div class="col-md-4">
                    <%: Html.ListBoxFor(model => model.Empleados, new MultiSelectList(ViewBag.Empleados, "Ci", "Nombre"), new { @id = "lstEmpleados", @class = "form-control" })%>
                    
                </div>
                <div class="col-md-4">
                    <button id="btnAddAll" type="button" onclick="addallItems();" class="btn btn-default btn-sm" >>></button>
                    <button id="btnAdd" type="button" onclick="addItem();" class="btn btn-default btn-sm" >></button>
                    <button id="btnRemove" type="button"   onclick="removeItem();" class="btn btn-default btn-sm"><</button>
                    <button id="btnRemoveAll"type="button"onclick="removeallItems();" class="btn btn-default btn-sm"><<</button>
                </div>
                <div class="col-md-4">
                    <%: Html.ListBoxFor(model => model.EmpleadosElegidos, Enumerable.Empty<SelectListItem>(), new { @class = "form-control" })%>
            
                </div>
            </div>
            </fieldset>
        </div>
        <p></p>
        <div class="form-group-lg">
            <fieldset>
            <legend><h4>Agregar Piezas a la Pieza</h4></legend>
            <div class="col-md-12">
              
               <%: Html.ListBoxFor(model => model.PiezasIntegradas, new SelectList(ViewBag.Piezas, "Id", "Nombre"), new { @class="form-control" })%>
           </div>
           <fieldset>
        </div>
        <div class="form-group">
            <div class="editor-label">
                <%: Html.LabelFor(model => model.TiempoProduccion)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.TiempoProduccion)%>
                <%: Html.ValidationMessageFor(model => model.TiempoProduccion)%>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" value="Crear" class="btn btn-success btn-lg" />
        </div>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</div>
<script type="text/javascript">
    $().ready(function () {

        $('#btnAdd').click(function () {
            $("#lstEmpleados option:selected").appendTo("#EmpleadosElegidos");
            $("#EmpleadosElegidos option").attr("selected", false);
            alert("Entro");
        });

        $('#btnAddAll').click(function () {
            $("#lstEmpleados option").appendTo("#EmpleadosElegidos");
            $("#EmpleadosElegidos option").attr("selected", false);

        });

        $('#btnRemove').click(function () {
            $("#EmpleadosElegidos option:selected").appendTo("#lstEmpleados");
            $("#lstEmpleados option").attr("selected", false);
        });
        $('#btnRemoveAll').click(function () {

            $("#lstEmpleados option:selected").appendTo("#EmpleadosElegidos");
            $("#EmpleadosElegidos option").attr("selected", false);
        });


        $('#btnAgregarPieza').click(function () {
            $("#PiezasIntegradas option:selected").appendTo("#PiezasElegidas");
            $("#PiezasElegidas option").attr("selected", false);
        });
    });

</script>
</asp:Content>
