﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.TipoEmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm("Create", "TipoEmpleado", FormMethod.Post, new {@role="form" }))
   { %>
    <%: Html.ValidationSummary(true)%>
    <fieldset>
        <legend>Nuevo Tipo de Empleado</legend>

        <div class="editor-label">
            <h3><%: Html.LabelFor(model => model.ValorHora,"Valor por hora")%></h3>
        </div>
        <div class="form-group">
            <div class="editor-field">
                <%: Html.EditorFor(model => model.ValorHora, new {@class="form-control" })%>
                <%: Html.ValidationMessageFor(model => model.ValorHora)%>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" value="Crear" class="btn btn-primary" />
        </div>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("<- Ir a inico>Tipo de Empleado", "Index", null, new { @class="btn btn-default" })%>
</div>

</asp:Content>
