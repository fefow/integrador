﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.TipoEmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>TipoEmpleadoModel</legend>

    <div class="display-label">Id</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Id) %>
    </div>

    <div class="display-label">Valor/Hora</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ValorHora) %>
    </div>
</fieldset>
<% using (Html.BeginForm("Delete","TipoEmpleado",FormMethod.Post)) { %>
    <p>
        <input type="submit" value="Delete" /> |
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>
<% } %>

</asp:Content>
