﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PruebaEF.Models.TipoEmpleadoModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    TipoEmpleado - Editar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Editar Tipo</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Tipo Empleado</legend>

        <%: Html.HiddenFor(model => model.Id) %>

        <div class="form-group"><h4>
            <%: Html.LabelFor(model => model.ValorHora) %>
            </h4>
        </div>
        <div class="form-group">
                <%: Html.EditorFor(model => model.ValorHora,new {@class="form-control"}) %>
            <%: Html.ValidationMessageFor(model => model.ValorHora) %>
        </div>

        <div class="form-group">
            <input type="submit" value="Save" class="btn btn-success" />
        </div>
    </fieldset>
<% } %>

<div class="form-group">
    <%: Html.ActionLink("<- Regresar a Tipo de Empleados", "Index", new { @class="btn btn-default"})%>
</div>

</asp:Content>
