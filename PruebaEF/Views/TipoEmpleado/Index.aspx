﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PruebaEF.Models.TipoEmpleadoModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>

<p>
    <%: Html.ActionLink("Crear nuevo tipo", "Create", null, new {@class="btn btn-primary" })%>
</p>
<table class="table table-striped">
    <tr>
        <th>
            Valor/Hora
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.ValorHora) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id = item.Id }, new { @class = "btn btn-default" })%>
            <%: Html.ActionLink("Detalles", "Details", new { id = item.Id }, new { @class = "btn btn-default" })%>
            <%: Html.ActionLink("Eliminar", "Delete", new { id = item.Id }, new { @class = "btn btn-default" })%>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
